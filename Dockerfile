FROM golang:alpine as pvr-build

COPY . /build

WORKDIR /build/gomodules/src/gitlab.com/pantacor/pvr
ENV GOPATH /build/gomodules

RUN apk update; apk add git
RUN pwd && ls -la && version=`git describe --tags` && sed -i "s/NA/$version/" version.go
RUN CGO_ENABLED=0 go get -d -v ./...
RUN CGO_ENABLED=0 go install -v ./...

FROM registry.gitlab.com/pantacor/pantavisor-runtime/pvlogger:X86_64-master as pvlogger

FROM alpine:3.8

COPY --from=pvlogger /usr/local/bin/pvlogger /usr/local/bin/

RUN apk update; apk add --update busybox-extras openrc ca-certificates wget jq dropbear-ssh dropbear dropbear-openrc lxc python3; \
	pip3 install httpie requests==2.18.1; \
	rc-update add dropbear default; \
	sed -i '/DROPBEAR_OPTS/c DROPBEAR_OPTS="-g -p :8022"' /etc/conf.d/dropbear; \
	rm -rf /var/cache/apk/*; \
	rm /etc/init.d/networking

ADD files /

RUN rc-update add zeronetworking default ; \
	rc-update add pvr-auto-follow default; \
	rc-update add pvlogger default; \
	rc-update add pv-user-meta-sync default ; \
	mkdir -p /root/.ssh; \
	echo root:pantasdk | chpasswd

WORKDIR /workspace
COPY --from=pvr-build /build/gomodules/bin/pvr /usr/local/bin/

ENTRYPOINT [ "/bin/sh" ]
